------------------------------------------------------------------------
r10 | myrroddin | 2020-03-17 02:40:45 +0000 (Tue, 17 Mar 2020) | 1 line
Changed paths:
   M /trunk/pkgmeta.yaml

- Oops typo in pkgmeta.yaml
------------------------------------------------------------------------
r8 | myrroddin | 2020-03-17 02:34:51 +0000 (Tue, 17 Mar 2020) | 2 lines
Changed paths:
   A /trunk/TSM_StringConverter.toc
   D /trunk/TradeSkillMaster_StringConverter.toc
   M /trunk/pkgmeta.yaml

- Rename the folder to stop TradeSkillMaster from complaining that this exists
- This forced a rename of the ToC file
------------------------------------------------------------------------

