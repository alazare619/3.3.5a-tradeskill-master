local _, addon = ...

local L = setmetatable({}, {__index = function(t, k)
    local v = tostring(k)
    rawset(t, k, v)
    return v
end})

addon.L = L

local LOCALE = GetLocale()

if LOCALE == "enUS" then
L["/tsmsc"] = "/tsmsc"
L["Insert itemIDs"] = "Insert itemIDs"
L["TradeSkillMaster itemID String Fixer"] = "TradeSkillMaster itemID String Fixer"
L["TSM String Converter"] = "TSM String Converter"
return end

if LOCALE == "deDE" then
--[[Translation missing --]]
--[[ L["/tsmsc"] = ""--]] 
--[[Translation missing --]]
--[[ L["Insert itemIDs"] = ""--]] 
--[[Translation missing --]]
--[[ L["TradeSkillMaster itemID String Fixer"] = ""--]] 
--[[Translation missing --]]
--[[ L["TSM String Converter"] = ""--]] 

return end

if LOCALE == "frFR" then
--[[Translation missing --]]
--[[ L["/tsmsc"] = ""--]] 
--[[Translation missing --]]
--[[ L["Insert itemIDs"] = ""--]] 
--[[Translation missing --]]
--[[ L["TradeSkillMaster itemID String Fixer"] = ""--]] 
--[[Translation missing --]]
--[[ L["TSM String Converter"] = ""--]] 

return end

if LOCALE == "esES" or LOCALE == "esMX" then
--[[Translation missing --]]
--[[ L["/tsmsc"] = ""--]] 
--[[Translation missing --]]
--[[ L["Insert itemIDs"] = ""--]] 
--[[Translation missing --]]
--[[ L["TradeSkillMaster itemID String Fixer"] = ""--]] 
--[[Translation missing --]]
--[[ L["TSM String Converter"] = ""--]] 

return end

if LOCALE == "ptBR" then
--[[Translation missing --]]
--[[ L["/tsmsc"] = ""--]] 
--[[Translation missing --]]
--[[ L["Insert itemIDs"] = ""--]] 
--[[Translation missing --]]
--[[ L["TradeSkillMaster itemID String Fixer"] = ""--]] 
--[[Translation missing --]]
--[[ L["TSM String Converter"] = ""--]] 

return end

if LOCALE == "ruRU" then
--[[Translation missing --]]
--[[ L["/tsmsc"] = ""--]] 
--[[Translation missing --]]
--[[ L["Insert itemIDs"] = ""--]] 
--[[Translation missing --]]
--[[ L["TradeSkillMaster itemID String Fixer"] = ""--]] 
--[[Translation missing --]]
--[[ L["TSM String Converter"] = ""--]] 

return end

if LOCALE == "koKR" then
--[[Translation missing --]]
--[[ L["/tsmsc"] = ""--]] 
--[[Translation missing --]]
--[[ L["Insert itemIDs"] = ""--]] 
--[[Translation missing --]]
--[[ L["TradeSkillMaster itemID String Fixer"] = ""--]] 
--[[Translation missing --]]
--[[ L["TSM String Converter"] = ""--]] 

return end

if LOCALE == "zhTW" then
--[[Translation missing --]]
--[[ L["/tsmsc"] = ""--]] 
--[[Translation missing --]]
--[[ L["Insert itemIDs"] = ""--]] 
--[[Translation missing --]]
--[[ L["TradeSkillMaster itemID String Fixer"] = ""--]] 
--[[Translation missing --]]
--[[ L["TSM String Converter"] = ""--]] 

return end

if LOCALE == "zhCN" then
--[[Translation missing --]]
--[[ L["/tsmsc"] = ""--]] 
--[[Translation missing --]]
--[[ L["Insert itemIDs"] = ""--]] 
--[[Translation missing --]]
--[[ L["TradeSkillMaster itemID String Fixer"] = ""--]] 
--[[Translation missing --]]
--[[ L["TSM String Converter"] = ""--]] 

return end

if LOCALE == "itIT" then
--[[Translation missing --]]
--[[ L["/tsmsc"] = ""--]] 
--[[Translation missing --]]
--[[ L["Insert itemIDs"] = ""--]] 
--[[Translation missing --]]
--[[ L["TradeSkillMaster itemID String Fixer"] = ""--]] 
--[[Translation missing --]]
--[[ L["TSM String Converter"] = ""--]] 

return end
