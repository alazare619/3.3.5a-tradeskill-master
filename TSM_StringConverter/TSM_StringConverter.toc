## Interface: 80300
## Title: |cff00ff00TradeSkillMaster Import String Converter|r
## Notes: Fixes invalid TSM import itemID group strings
## Version: 8.3-release2
## Author: Myrroddin/Sygon
## X-Revision: 10
## X-Copyright: Copyright 2019 - 2020 Paul Vandersypen. All Rights Reserved
## X-Date: 2020-03-17T2:40:45Z
## OptionalDeps: LibStub, CallbackHandler-1.0, Ace3
## X-Category: Auction & Economy

#@no-lib-strip@
LibStub\LibStub.lua
CallbackHandler-1.0\CallbackHandler-1.0.xml
AceGUI-3.0\AceGUI-3.0.xml
#@end-no-lib-strip@

Localization.lua
Main.lua
