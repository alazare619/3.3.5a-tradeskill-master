local L = LibStub("AceLocale-3.0"):NewLocale("TradeSkillMaster_Vendor", "ruRU")
if not L then return end

L["Opens the Vendor frame."] = true
L["Nothing to sell in your bags."] = true
L["%sRight-Click|r to ignore a item for this session. %sShift + Right-Click|r to ignore permanently. You can undo this in the Vendor options."] = true
L["General Options"] = true
L["Ignored Item"] = true
L["Ignored Items"] = true
L["Ignoring all %s permanently. You can undo this in the Vendor options."] = true
L["Ignoring all %s this session (until your UI is reloaded)."] = true
L["Item"] = true
L["Options"] = true
L["Removed %s from the permanent ignore list."] = true
L["Result"] = true
L["Right click on this row to remove this item from the permanent ignore list."] = true
L["Operation"] = true
L["Sell Next"] = true
L["Sell All"] = true
L["Selling %s. Reason: %s."] = true
L["Price"] = true
L["Ignoring %s permanently. You can undo this in the TSM Vendor options."] = true
L["Ignoring %s this session (until your UI is reloaded)."] = true
L["Sell items Automatically (CAUTION!!!)"] = true
L["If checked, the items will be sold automatically. USE WITH EXTREME CAUTION!!!"] = true
L["Sell gray items automatically"] = true
L["If checked, all gray items will be sold automatically."] = true
L["Select the maximum item quality to sell."] = true
L["Sell Expired Items"] = true
L["If checked, will sell items with the number of expired auctions above the limit."] = true
L["Minimum expired auction before selling"] = true
L["Will sell only after the specified number of expired auction after the last sell."] = true
L["Sell all soubound items."] = true
L["If checked, soulbound items will be sold. Useful if you cannot disenchant them."] = true
L["Max Soulbound Quality to Sell"] = true
L["Select the maximum item quality to sell."] = true
L["Max Quality to Sell"] = true
L["SoulBound"] = true
L["Selling items up to %s"] = true
L["%s or with %d or more expired auctions"] = true
L["Sold %d item(s) for %s."] = true
L["Sold %d items for %s."] = true
L["Expired Auctions"] = true
L["Quality"] = true
L["Qty"] = true
L["Gray"] = true
L["Common"] = true
L["Uncommon"] = true
L["Rare"] = true
L["Epic"] = true
L["Legendary"] = true
L["Artifact"] = true
L["Heirloom"] = true
L["WoW Token"]= true
L["Show suggestions with the toolip"] = true
L["If checked, a suggestion will be shown in the tooltip."] = true
L["Vendor"] = true
L["Auction"] = true
L["Disenchant"] = true
L["Mill"] = true
L["Prospect"] = true
L["Suggestion"] = true
L["Suggests the item action to be taken based on prices."] = true
L["If checked, the suggestion will be shown."] = true
L["Do nothing"] = true
L["Only sell items with Suggestion: %sVendor%s"] = true
L["If checked, only items with Vendor Suggestions will be sold. This avoids selling an item with higher disenchanting value."] = true
L["Display details about the suggestion."] = true
L["If checked, the details about the suggestion will be shown."] = true
L["Minimum Post Options"] = true
L["Sell items bellow the minimum post price"] = true
L["If checked, will sell items when its market price is bellow the minimum post price from Auction operations."] = true
L["Maximum percent from minimim post to sell the item"] = true
L["Will sell only if the market price if bellow this percentage from minimim post price."] = true
L["Bellow Min Post"] = true              
L["Auctioning Prices:"] = "Цены аукциона:"
