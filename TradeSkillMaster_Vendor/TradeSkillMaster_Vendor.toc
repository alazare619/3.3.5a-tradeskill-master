## Interface: 60100
## Title: |cff00ff00TradeSkillMaster Vendor|r
## Notes: Module for TSM for creating Vendor operations and auto sell items (gray, too many expired auctions, so on) 
## Author: Zotwee
## Version: 1.0.8
## SavedVariables: TradeSkillMaster_VendorDB
## Dependency: TradeSkillMaster
## X-Curse-Packaged-Version: v1.0.8
## X-Curse-Project-Name: TradeSkillMaster_Vendor
## X-Curse-Project-ID: tradeskillmaster_vendor
## X-Curse-Repository-ID: wow/tradeskillmaster_vendor/mainline

Locale\enUS.lua
Locale\ptBR.lua
Locale\deDE.lua
Locale\esES.lua
Locale\esMX.lua
Locale\frFR.lua
Locale\koKR.lua
Locale\ruRU.lua
Locale\zhCN.lua
Locale\zhTW.lua

TradeSkillMaster_Vendor.lua

Modules\Options.lua
Modules\Gui.lua
Modules\Suggestion.lua

