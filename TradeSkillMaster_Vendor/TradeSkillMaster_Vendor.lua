--[[

TradeSkillMaster Vendor addon
http://zotwee.info/

]]

local TSM = select(2, ...)
TSM = LibStub("AceAddon-3.0"):NewAddon(TSM, "TSM_Vendor", "AceEvent-3.0", "AceConsole-3.0")
local L = LibStub("AceLocale-3.0"):GetLocale("TradeSkillMaster_Vendor") -- loads the localization table
TSM.version = GetAddOnMetadata("TradeSkillMaster_Vendor", "Version")
TSM.moduleName = "TradeSkillMaster Vendor"

local r,g,b  = 1, 1, 0
local WHITE  = "|cFFFFFFFF"
local YELLOW = "|cFFFFFF00"
local GREEN  = "|cFF00FF00"
local RED    = "|cFFFF0000"
local BLUE   = "|cFF5A93CC"
local CLEAR  = "|r"

local qualityText = {}
qualityText[0] = "|cFF9D9D9D"..L["Gray"].."|r"
qualityText[1] = "|cFFFFFFFF"..L["Common"].."|r"
qualityText[2] = "|cFF1EFF00"..L["Uncommon"].."|r"
qualityText[3] = "|cFF0070DD"..L["Rare"].."|r"
qualityText[4] = "|cFFA335EE"..L["Epic"].."|r"
qualityText[5] = "|cFFFF8000"..L["Legendary"].."|r"
qualityText[6] = "|cFFE6CC80"..L["Artifact"].."|r"
qualityText[7] = "|cFFE6CC80"..L["Heirloom"].."|r"
qualityText[8] = "|cFF00CCFF"..L["WoW Token"].."|r"

function TSM:QualityText(quality)
  return qualityText[quality]
end

local savedDBDefaults = {
  global = {
    autoSell = false,
    autoSellGray = true,
    sellSouldBound = false,
    maxSoulBoundQuality = 2,
    ignore = {},
    optionsTreeStatus = {},
  },
  char = {
    autoSell = false,
    autoSellGray = true,
    sellSoulBound = false,
    maxSoulBoundQuality = 2,
    showSuggestion = true,
    detailsSuggestion = true,
    onlyVendor = false,
    migrated = false,
  }
}

local private = { data = {}, ignore = {} }

local function migrateDB()
  if TSM.db.char.migrated then return end
  print("Migrating DB...")
  TSM.db.char.autoSell = TSM.db.global.autoSell
  TSM.db.char.autoSellGray = TSM.db.global.autoSellGray
  TSM.db.char.sellSoulBound = TSM.db.global.sellSoulBound
  TSM.db.char.maxSoulBoundQuality = TSM.db.global.maxSoulBoundQuality
  TSM.db.char.migrated = true
end

-- Called once the player has loaded WOW.
function TSM:OnInitialize()
  TSM.db = LibStub:GetLibrary("AceDB-3.0"):New("TradeSkillMaster_VendorDB", savedDBDefaults, true)
  migrateDB()
  for moduleName, module in pairs(TSM.modules) do
    TSM[moduleName] = module
  end

  -- register this module with TSM
  TSM:RegisterModule()
end

-- registers this module with TSM by first setting all fields and then calling TSMAPI:NewModule().
function TSM:RegisterModule()
  TSM.operations = 
  {
    maxOperations=11,
    callbackOptions="Options:Load",
    callbackInfo="GetOperationInfo"
  }
  
  TSM.moduleAPIs = {
    {key="Vendor", callback="Options:Load"},
  }

  TSM.tooltipOptions = { callback = "Suggestion:LoadTooltipOptions" }

  TSMAPI:NewModule(TSM)
end

TSM.operationDefaults = {
  sellExpired = false,
  numberExpired = 10,
  maxExpiredQuality = 1,
  maxSellQuality = 1,
  sellMinPost = false,
  percMinPost = 50,
  ignorePlayer = {},
  ignoreFactionrealm = {},
  relationships = {},
}

function TSM:GetOperationInfo(operationName)
  local operation = TSM.operations[operationName]
  if not operation then return end
  if operation.target == "" then return end
  
  local msg = format(L["Selling items up to %s"], TSM:QualityText(operation.maxSellQuality))
  if operation.sellExpired then
    msg = format(L["%s or with %d or more expired auctions"], msg, operation.numberExpired)
  end
  return msg
end


function TSM:GetPrice(customPrice, itemString)
  local price, err
  if type(customPrice) == "number" then
    price = customPrice
  elseif type(customPrice) == "string" then
    local func, parseErr = TSMAPI:ParseCustomPrice(customPrice)
    err = parseErr
    price = func and func(itemString)
  end
  return price ~= 0 and price or nil, err
end

function TSM:GetTooltip(itemString, quantity)
  return TSM.Suggestion:GetTooltip(itemString, quantity)
end
