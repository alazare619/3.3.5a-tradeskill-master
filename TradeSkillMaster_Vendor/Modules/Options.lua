--[[

TradeSkillMaster Vendor addon
http://zotwee.info/

]]
local TSM = select(2, ...)
local Options = TSM:NewModule("Options")
local AceGUI = LibStub("AceGUI-3.0") -- load the AceGUI libraries

local L = LibStub("AceLocale-3.0"):GetLocale("TradeSkillMaster_Vendor")
local ignoreST

function Options:Load(parent, operation, group)
  Options.treeGroup = AceGUI:Create("TSMTreeGroup")
  Options.treeGroup:SetLayout("Fill")
  Options.treeGroup:SetCallback("OnGroupSelected", function(...) Options:SelectTree(...) end)
  Options.treeGroup:SetStatusTable(TSM.db.global.optionsTreeStatus)
  parent:AddChild(Options.treeGroup)

  Options:UpdateTree()
  if operation then
    if operation == "" then
      Options.currentGroup = group
      Options.treeGroup:SelectByPath(2)
      Options.currentGroup = nil
    else
      Options.treeGroup:SelectByPath(2, operation)
    end
  else
    Options.treeGroup:SelectByPath(1)
  end
end

function Options:UpdateTree()
  local operationTreeChildren = {}

  for name in pairs(TSM.operations) do
    tinsert(operationTreeChildren, { value = name, text = name })
  end
  
  sort(operationTreeChildren, function(a, b) return a.value < b.value end)

  Options.treeGroup:SetTree({ { value = 1, text = "Options" }, { value = 2, text = "Operations", children = operationTreeChildren } })
end

function Options:SelectTree(treeGroup, _, selection)
  treeGroup:ReleaseChildren()

  local major, minor = ("\001"):split(selection)
  major = tonumber(major)
  if major == 1 then
    Options:LoadGeneralSettings(treeGroup)
  elseif minor then
    Options:DrawOperationSettings(treeGroup, minor)
  else
    Options:DrawNewOperation(treeGroup)
  end
end

function Options:DrawNewOperation(container)
  local currentGroup = Options.currentGroup
  local page = {
    {
      type = "ScrollFrame",
      layout = "List",
      children = {
        {
          type = "InlineGroup",
          layout = "flow",
          title = "New Operation",
          children = {
            {
              type = "Label",
              text = "Vendor operations allow for quick and easy vendoring of items.",
              relativeWidth = 1,
            },
            {
              type = "EditBox",
              label = "Operation Name",
              relativeWidth = 0.8,
              callback = function(self, _, name)
                name = (name or ""):trim()
                if name == "" then return end
                if TSM.operations[name] then
                  self:SetText("")
                  return TSM:Printf("Error creating operation. Operation with name '%s' already exists.", name)
                end
                TSM.operations[name] = CopyTable(TSM.operationDefaults)
                Options:UpdateTree()
                Options.treeGroup:SelectByPath(2, name)
                TSMAPI:NewOperationCallback("Vendor", currentGroup, name)
              end,
              tooltip = "Give the new operation a name. A descriptive name will help you find this operation later.",
            },
          },
        },
      },
    },
  }
  TSMAPI:BuildPage(container, page)
end


function Options:DrawOperationSettings(container, operationName)
  local tg = AceGUI:Create("TSMTabGroup")
  tg:SetLayout("Fill")
  tg:SetFullHeight(true)
  tg:SetFullWidth(true)
  tg:SetTabs({{value=1, text="General"}, {value=2, text="Relationships"}, {value=3, text="Management"}})
  tg:SetCallback("OnGroupSelected", function(self,_,value)
      tg:ReleaseChildren()
      TSMAPI:UpdateOperation("Vendor", operationName)
      if value == 1 then
        Options:DrawOperationGeneral(self, operationName)
      elseif value == 2 then
        Options:DrawOperationRelationships(self, operationName)
      elseif value == 3 then
        TSMAPI:DrawOperationManagement(TSM, self, operationName)
      end
    end)
  container:AddChild(tg)
  tg:SelectTab(1)
end

function Options:DrawOperationGeneral(container, operationName)
  local operationSettings = TSM.operations[operationName]
  if operationSettings.percMinPost == nil then
    operationSettings.percMinPost = 50
  end
  
  local qualityList = {};
  for i=0, 8, 1 do
    qualityList[i] = TSM:QualityText(i)
  end
  
  local page = {
    {
      type = "ScrollFrame",
      layout = "Flow",
      fullHeight = true,
      children = {
        {
          type = "InlineGroup",
          title = L["General Options"],
          layout = "Flow",
          children = {
            {
              type = "Dropdown",
              label = L["Max Quality to Sell"],
              relativeWidth = 0.5,
              list = qualityList,
              settingInfo = {operationSettings, "maxSellQuality"},
              tooltip = L["Select the maximum item quality to sell."],
              callback = function(self)
                TSM.GUI:UpdateST()
              end,
            },
          },
        },
        {
          type = "InlineGroup",
          title = L["Minimum Post Options"],
          layout = "Flow",
          children = {
            {
              type = "CheckBox",
              label = L["Sell items bellow the minimum post price"],
              relativeWidth = 1,
              settingInfo = {operationSettings, "sellMinPost"},
              tooltip = L["If checked, will sell items when its market price is bellow the minimum post price from Auction operations."],
              callback = function(self)
                TSM.GUI:UpdateST()
              end,
            },
            {
              type = "Slider",
              label = L["Maximum percent from minimim post to sell the item"],
              relativeWidth = 0.49,
              min = 1,
              max = 100,
              step = 1,
              settingInfo = {operationSettings, "percMinPost"},
              --disabled = not operationSettings.sellMinPost,
              tooltip = L["Will sell only if the market price if bellow this percentage from minimim post price."],
              callback = function(self)
                TSM.GUI:UpdateST()
              end,
            },
          },
        },
        {
          type = "InlineGroup",
          title = L["Expired Auctions"],
          layout = "Flow",
          children = {
            {
              type = "CheckBox",
              label = L["Sell Expired Items"],
              relativeWidth = 0.49,
              settingInfo = {operationSettings, "sellExpired"},
              tooltip = L["If checked, will sell items with the number of expired auctions above the limit."],
              callback = function(self)
                TSM.GUI:UpdateST()
              end,
            },
            {
              type = "Slider",
              label = L["Minimum expired auction before selling"],
              relativeWidth = 0.49,
              min = 1,
              max = 100,
              step = 1,
              settingInfo = {operationSettings, "numberExpired"},
              --disabled = not operationSettings.sellExpired,
              tooltip = L["Will sell only after the specified number of expired auction after the last sell."],
              callback = function(self)
                TSM.GUI:UpdateST()
              end,
            },
            {
              type = "Dropdown",
              label = L["Max Quality to Sell"],
              relativeWidth = 0.5,
              list = qualityList,
              settingInfo = {operationSettings, "maxExpiredQuality"},
              --disabled = not operationSettings.sellExpired,
              tooltip = L["Select the maximum item quality to sell."],
              callback = function(self)
                TSM.GUI:UpdateST()
              end,
            },
            
          },
        },
      },
    },
  }

  TSMAPI:BuildPage(container, page)
end

function Options:DrawOperationRelationships(container, operationName)
  local settingInfo = {
    {
      label = "General Settings",
    },
  }
  TSMAPI:ShowOperationRelationshipTab(TSM, container, TSM.operations[operationName], settingInfo)
end

function Options:LoadGeneralSettings(container)
  local qualityList = {};
  for i=0, 8, 1 do
    qualityList[i] = TSM:QualityText(i)
  end

  local page = {
    {
      type = "ScrollFrame",
      layout = "List",
      children = {
        {
          type = "InlineGroup",
          layout = "flow",
          title = "General Settings",
          relativeWidth = 1,
          children = {
            {
              type = "CheckBox",
              label = L["Sell gray items automatically"],
              relativeWidth = 0.49,
              settingInfo = {TSM.db.char, "autoSellGray"},
              tooltip = L["If checked, all gray items will be sold automatically."],
              callback = function(self)
                TSM.GUI:UpdateST()
              end,
            },
            {
              type = "CheckBox",
              label = L["Sell items Automatically (CAUTION!!!)"],
              relativeWidth = 0.49,
              settingInfo = {TSM.db.char, "autoSell"},
              tooltip = L["If checked, the items will be sold automatically. USE WITH EXTREME CAUTION!!!"],
            },
            {
              type = "CheckBox",
              label = format(L["Only sell items with Suggestion: %sVendor%s"], "|cFF00FF00", "|r"),
              relativeWidth = 1,
              settingInfo = {TSM.db.char, "onlyVendor"},
              tooltip = L["If checked, only items with Vendor Suggestions will be sold. This avoids selling an item with higher disenchanting value."],
              callback = function(self)
                TSM.GUI:UpdateST()
              end,
            },
            {
              type = "CheckBox",
              label = L["Sell all soubound items."],
              relativeWidth = 0.49,
              settingInfo = {TSM.db.char, "sellSoulBound"},
              tooltip = L["If checked, soulbound items will be sold. Useful if you cannot disenchant them."],
            },
            {
              type = "Dropdown",
              label = L["Max Soulbound Quality to Sell"],
              relativeWidth = 0.5,
              list = qualityList,
              settingInfo = {TSM.db.char, "maxSoulBoundQuality"},
              tooltip = L["Select the maximum item quality to sell."],
              callback = function(self)
                TSM.GUI:UpdateST()
              end,
            },
          },
        },
      },
    },
  }

  TSMAPI:BuildPage(container, page)
  local stParent = container.children[1].children[1]

  local container2 = AceGUI:Create("SimpleGroup")
  container2:SetLayout("Fill")
  container2.frame:SetParent(stParent.frame)
  stParent:AddChild(container2)
  --container2.frame:SetPoint("TOPLEFT", 5, -5)
  --container2.frame:SetPoint("BOTTOMRIGHT", -5, -5)
        
  if not ignoreST then
    local stCols = {
      {
        name = L["Ignored Items"],
        width = 1,
      }
    }
    local handlers = {
      OnEnter = function(_, data, self)
        if not data.itemString then return end
        GameTooltip:SetOwner(self, "ANCHOR_NONE")
        GameTooltip:SetPoint("BOTTOMLEFT", self, "TOPLEFT")
        GameTooltip:AddLine(L["Right click on this row to remove this item from the permanent ignore list."], 1, 1, 1, true)
        GameTooltip:Show()
      end,
      OnLeave = function()
        GameTooltip:ClearLines()
        GameTooltip:Hide()
      end,
      OnClick = function(_, data, _, button)
        if not data.itemString then return end
        if button == "RightButton" then
          TSM.db.global.ignore[data.itemString] = nil
          TSM:Printf(L["Removed %s from the permanent ignore list."], data.link)
          Options:UpdateIgnoreST()
        end
      end
    }
    ignoreST = TSMAPI:CreateScrollingTable(container2.frame, stCols, handlers, 20)
  end

  ignoreST:Show()
  ignoreST:SetParent(container2.frame)
  ignoreST:SetAllPoints()

  Options:UpdateIgnoreST()
end

function Options:UpdateIgnoreST()
	if not ignoreST or not ignoreST:IsVisible() then return end
	local stData = {}
	for itemString in pairs(TSM.db.global.ignore) do
		local name, link = TSMAPI:GetSafeItemInfo(itemString)
		name = name or itemString
		link = link or itemString
		local row = {
			cols = {
				{
					value = link,
				},
			},
			name = name,
			link = link,
			itemString = itemString,
		}
		tinsert(stData, row)
	end
	sort(stData, function(a, b) return a.name < b.name end)
	ignoreST:SetData(stData)
	--GUI:UpdateST()
end
