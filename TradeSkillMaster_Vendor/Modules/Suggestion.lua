--[[

TradeSkillMaster Vendor addon
http://zotwee.info/

]]
local TSM = select(2, ...)
local Suggestion = TSM:NewModule("Suggestion")
local AceGUI = LibStub("AceGUI-3.0") -- load the AceGUI libraries
local L = LibStub("AceLocale-3.0"):GetLocale("TradeSkillMaster_Vendor")

local WHITE  = "|cFFFFFFFF"
local YELLOW = "|cFFFFFF00"
local GREEN  = "|cFF00FF00"
local RED    = "|cFFFF0000"
local CLEAR  = "|r"

Suggestion.NONE       = 0
Suggestion.DISENCHANT = 1
Suggestion.MILL       = 2
Suggestion.PROSPECT   = 3
Suggestion.VENDOR     = 4
Suggestion.AUCTION    = 5

function Suggestion:GetMarketData(itemID, key)
  if type(itemID) ~= "number" then itemID = TSMAPI:GetItemID(itemID) end

  local data = ZotweeMarketItem(itemID)

  if not data then return end

  if not key then return data end

  if data[key] and data[key] > 0 then
    return data[key]
  else
    return
  end
end

function Suggestion:GetSuggestion(itemString)
  local data = {
    disenchant = nil,
    mill = nil,
    prospect = nil,
    vendor = nil,
    auction = nil,
    winner = 0,
    suggestion = Suggestion.NONE,
  }
  if not itemString then return data end
  if not strfind(itemString, "item:") then return data end
  local itemID = TSMAPI:GetItemID(itemString)
  if not itemID then return data end

  local suggestion = Suggestion.NONE
  local deValue = Suggestion:GetDisenchantValue(itemString)
  local millValue = Suggestion:GetMillValue(itemString)
  local prospectValue = Suggestion:GetProspectValue(itemString)
  local vendorValue = select(11, TSMAPI:GetSafeItemInfo(itemString))
      
  if vendorValue and vendorValue > 0 then
    data.winner = vendorValue * 0.95
    data.vendor = vendorValue
    data.suggestion = Suggestion.VENDOR
  end
  
  -- Can't auction a sould bound item
  local bind = TSMAPI:IsSoulbound(itemid)   
  if not bind then
    local auctionPrice
    if IsAddOnLoaded("TradeSkillMaster_AuctionDB") then
      auctionPrice = TSMAPI:GetItemValue(itemString, "DBMarket")
    end
        
    if auctionPrice and auctionPrice > 0 then
      data.auction = auctionPrice
      if auctionPrice > data.winner then
        data.winner = auctionPrice
        data.suggestion = Suggestion.AUCTION
      end
    end
  end

  if deValue and deValue > 0 then
    data.disenchant = deValue
    if deValue > data.winner then
      data.winner = deValue
      data.suggestion = Suggestion.DISENCHANT
    end
  end
      
  if millValue and millValue > 0 then
    data.mill = millValue 
    if millValue > data.winner then
      data.winner = millValue
      data.suggestion = Suggestion.MILL
    end
  end
  
  if prospectValue and prospectValue > 0 then
    data.prospect = prospectValue
    if prospectValue > data.winner then
      data.winner = prospectValue
      data.suggestion = Suggestion.PROSPECT
    end
  end
  
  return data

end

function Suggestion:SuggestionText(suggestion)
  if suggestion == Suggestion.VENDOR then return L["Vendor"] end
  if suggestion == Suggestion.AUCTION then return L["Auction"] end
  if suggestion == Suggestion.DISENCHANT then return L["Disenchant"] end
  if suggestion == Suggestion.MILL then return L["Mill"] end
  if suggestion == Suggestion.PROSPECT then return L["Prospect"] end
  
  return L["Do nothing"]
end

function Suggestion:GetTooltip(itemString, quantity)
  if not TSM.db.char.showSuggestion then return end
  if not strfind(itemString, "item:") then return end
  local itemID = TSMAPI:GetItemID(itemString)
  if not itemID then return end
  local text = {}
  
  local data = Suggestion:GetSuggestion(itemString)

  if TSM.db.char.detailSuggestion then      
    if data.vendor then
  		tinsert(text, {left = "  "..L["Vendor"]..": "..CLEAR, right = TSMAPI:FormatTextMoney(data.vendor, "|cffffffff", true)})
  	end
  	
    if data.auction then
      tinsert(text, {left = "  "..L["Auction"]..": "..CLEAR, right = TSMAPI:FormatTextMoney(data.auction, "|cffffffff", true)})
    end
  
    if data.disenchant then
      tinsert(text, {left = "  "..L["Disenchant"]..": "..CLEAR, right = TSMAPI:FormatTextMoney(data.disenchant, "|cffffffff", true)})
    end
        
    if data.mill then
      tinsert(text, {left = "  "..L["Mill"]..": "..CLEAR, right = TSMAPI:FormatTextMoney(data.mill, "|cffffffff", true)})
    end
    
    if data.prospect then
      tinsert(text, {left = "  "..L["Prospect"]..": "..CLEAR, right = TSMAPI:FormatTextMoney(data.prospect, "|cffffffff", true)})
    end
  end

  if data.winner then
    tinsert(text, {left = "  "..L["Suggestion"]..":", right = GREEN..Suggestion:SuggestionText(data.suggestion)..CLEAR})
  end
      
  if #text > 0 then
    tinsert(text, 1, { left = "|cffffff00" .. "TSM Vendor:" })
    return text
  end
end

function Suggestion:GetDisenchantValue(itemString)
	local itemID = TSMAPI:GetItemID(itemString)
	local name, link, quality, ilvl, reqLevel, iType, subclass, maxStack, equipSlot, texture, vendorPrice = GetItemInfo(itemID)	
	local WEAPON, ARMOR = GetAuctionItemClasses()

	if not itemString or TSMAPI.DisenchantingData.notDisenchantable[itemString] or not (iType == ARMOR or iType == WEAPON) then return end

	local value = 0
	for _, data in ipairs(TSMAPI.DisenchantingData.disenchant) do
		for item, itemData in pairs(data) do
			if item ~= "desc" and itemData.itemTypes[iType] and itemData.itemTypes[iType][quality] then
				for _, deData in ipairs(itemData.itemTypes[iType][quality]) do
					if ilvl >= deData.minItemLevel and ilvl <= deData.maxItemLevel then
						local matValue = TSMAPI:GetItemValue(item, "DBMarket")
						value = value + (matValue or 0) * deData.amountOfMats
					end
				end
			end
		end
	end

	value = floor(value)
	return value > 0 and value or nil
end

function Suggestion:GetMillValue(itemString)
	local value = 0
	
	for _, targetItem in ipairs(TSMAPI:GetConversionTargetItems("mill")) do
		local herbs = TSMAPI:GetItemConversions(targetItem)
		if herbs[itemString] then
			local matValue = TSMAPI:GetItemValue(targetItem, "DBMarket")
			value = value + (matValue or 0) * herbs[itemString].rate
		end
	end
	
	value = floor(value)
	return value > 0 and value or nil
end

function Suggestion:GetProspectValue(itemString)
	local value = 0
	
	for _, targetItem in ipairs(TSMAPI:GetConversionTargetItems("prospect")) do
		local gems = TSMAPI:GetItemConversions(targetItem)
		if gems[itemString] then
			local matValue = TSMAPI:GetItemValue(targetItem, "DBMarket")
			value = value + (matValue or 0) * (gems[itemString].rate /5)
		end
	end
	
	value = floor(value)
	return value > 0 and value or nil
end

function Suggestion:LoadTooltipOptions(container)
  local page = {
    {
      type = "SimpleGroup",
      layout = "Flow",
      fullHeight = true,
      children = {
        {
          type = "CheckBox",
          label = L["Suggests the item action to be taken based on prices."],
          settingInfo = { TSM.db.char, "showSuggestion" },
          tooltip = L["If checked, the suggestion will be shown."],
        },
        {
          type = "CheckBox",
          label = L["Display details about the suggestion."],
          disabled = not TSM.db.char.showSuggestion,
          settingInfo = { TSM.db.char, "detailSuggestion" },
          tooltip = L["If checked, the details about the suggestion will be shown."],
        },
      },
    },
  }

  TSMAPI:BuildPage(container, page)
end
