--[[

TradeSkillMaster Vendor addon
http://zotwee.info/

]]
local TSM = select(2, ...)
local GUI = TSM:NewModule("GUI", "AceEvent-3.0")

local L = LibStub("AceLocale-3.0"):GetLocale("TradeSkillMaster_Vendor")

local private = { data = {}, ignore = {} }
TSMAPI:RegisterForTracing(private, "TSM_Vendor.GUI_private")

function GUI:OnEnable()
	private.frame = private:CreateVendorFrame()
	TSMAPI:CreateEventBucket("BAG_UPDATE", function() private:UpdateST() end, 1)
	GUI:RegisterEvent("MERCHANT_SHOW", private.ShowFrame)
	GUI:RegisterEvent("MERCHANT_CLOSED", private.HideFrame)
	--private.ShowFrame()
end

function GUI:ShowFrame()
	private.hidden = nil
	private:UpdateST(true)
end

function private:ShowFrame()
  private.hidden = nil
  private:UpdateST(true)
end

function private:HideFrame()
  private.hidden = true
  private.frame:Hide()
end

function GUI:UpdateST(fromVendor)
	private:UpdateST(fromVendor)
end

function private:CreateVendorFrame()
  local frameDefaults = {
    x = 500,
    y = 450,
    width = 500,
    height = 400,
    scale = 1,
  }
  local frame = TSMAPI:CreateMovableFrame("TSMVendorFrame", frameDefaults)
 
	frame:SetFrameStrata("HIGH")
	TSMAPI.Design:SetFrameBackdropColor(frame)

	local title = TSMAPI.GUI:CreateLabel(frame)
	title:SetText("TSM_Vendor")
	title:SetPoint("TOPLEFT")
	title:SetPoint("TOPRIGHT")
	title:SetHeight(20)

	local line = TSMAPI.GUI:CreateVerticalLine(frame, 0)
	line:ClearAllPoints()
	line:SetPoint("TOPRIGHT", -25, -1)
	line:SetWidth(2)
	line:SetHeight(22)

	local closeBtn = TSMAPI.GUI:CreateButton(frame, 18)
	closeBtn:SetPoint("TOPRIGHT", -3, -3)
	closeBtn:SetWidth(19)
	closeBtn:SetHeight(19)
	closeBtn:SetText("X")
	closeBtn:SetScript("OnClick", function()
		if InCombatLockdown() then return end
		private.hidden = true
		frame:Hide()
	end)

	TSMAPI.GUI:CreateHorizontalLine(frame, -23)

	local stContainer = CreateFrame("Frame", nil, frame)
	stContainer:SetPoint("TOPLEFT", 0, -25)
	stContainer:SetPoint("BOTTOMRIGHT", 0, 70)
	TSMAPI.Design:SetFrameColor(stContainer)

	local stCols = {
		{
			name = L["Item"],
			width = 0.4,
		},
		{
      name = L["Price"],
      width = 0.23,
      align = "RIGHT",
    },
    {
      name = L["Qty"],
      width = 0.05,
      align = "RIGHT",
    },
    {
      width = 0.02,
    },
		{
			name = L["Operation"],
			width = 0.32,
		},
	}
	local handlers = {
		OnClick = function(_, data, self, button)
			if not data then return end
			if button == "RightButton" then
				if IsShiftKeyDown() then
					TSM.db.global.ignore[data.itemString] = true
					TSM:Printf(L["Ignoring %s permanently. You can undo this in the TSM Vendor options."], data.link)
					TSM.Options:UpdateIgnoreST()
				else
					private.ignore[data.itemString] = true
					TSM:Printf(L["Ignoring %s this session (until your UI is reloaded)."], data.link)
				end
				private:UpdateST()
			end
		end,
		OnEnter = function(_, data, self)
			if not data then return end
			local color = TSMAPI.Design:GetInlineColor("link")
			GameTooltip:SetOwner(self, "ANCHOR_RIGHT")
      GameTooltip:SetHyperlink(data.link)
			GameTooltip:Show()
		end,
		OnLeave = function()
			GameTooltip:ClearLines()
			GameTooltip:Hide()
		end
	}
	local st = TSMAPI:CreateScrollingTable(stContainer, stCols, handlers, 12)
	st:SetData({})
	st:DisableSelection(true)
	frame.st = st

        local color = TSMAPI.Design:GetInlineColor("link")

        local title = TSMAPI.GUI:CreateLabel(frame)
        title:SetText(format(L["%sRight-Click|r to ignore a item for this session. %sShift + Right-Click|r to ignore permanently. You can undo this in the Vendor options."], color, color))
        title:SetPoint("BOTTOMLEFT", 5, 30)
        title:SetPoint("BOTTOMRIGHT")
        title:SetJustifyH("LEFT")

	local sellNextBtn = TSMAPI.GUI:CreateButton(frame, 14, "TSMSellNextButton", true)
	sellNextBtn:SetPoint("BOTTOMLEFT", 80, 3)
	sellNextBtn:SetWidth(150)
	sellNextBtn:SetHeight(20)
	sellNextBtn:SetText(L["Sell Next"])
	sellNextBtn:SetScript("PreClick", function()
		if sellNextBtn:IsVisible() and #private.data > 0 then
			local data = private.data[1]
			private.tempData = data
			TSM:Printf(L["Selling %s. Reason: %s."], data.link, data.cols[5].value)
      UseContainerItem(data.bag, data.slot)
      if data.vendorSell and data.vendorSell > 0 then
        TSM:Print(format(L["Sold %d item(s) for %s."], data.quantity, TSMAPI:FormatTextMoneyIcon(data.vendorSell, "|cffffffff", true)))
      end
		end
	end)
	frame.sellNextBtn = sellNextBtn

  local sellAllBtn = TSMAPI.GUI:CreateButton(frame, 14, "TSMSellAllButton", true)
  sellAllBtn:SetPoint("BOTTOMRIGHT", -100, 3)
  sellAllBtn:SetWidth(150)
  sellAllBtn:SetHeight(20)
  sellAllBtn:SetText(L["Sell All"])
  sellAllBtn:SetScript("PreClick", function()
    if sellAllBtn:IsVisible() and #private.data > 0 then
      local soldQty = 0
      local soldTotal = 0
      for i = 1 , #private.data, 1 do
        local data = private.data[i]
        private.tempData = data
        TSM:Printf(L["Selling %s. Reason: %s."], data.link, data.cols[5].value)
        UseContainerItem(data.bag, data.slot)
        soldQty = soldQty + data.quantity
        soldTotal = soldTotal + data.vendorSell
      end
      if soldTotal and soldTotal > 0 then
        TSM:Print(format(L["Sold %d items for %s."], soldQty, TSMAPI:FormatTextMoneyIcon(soldTotal, "|cffffffff", true)))
      end
    end
  end)
  frame.sellAllBtn = sellAllBtn

	return frame
end

function private:SellItem(bag, slot, reason)
  local link = GetContainerItemLink(bag, slot)
  TSM:Printf(L["Selling %s. Reason: %s."], link, reason)
  UseContainerItem(bag, slot)
end

local function GetItemPrice(operationPrice, itemString)
  local func = TSMAPI:ParseCustomPrice(operationPrice)
  local price = func and func(itemString)
  return price ~= 0 and price or nil
end

local function GetMinPost(link)
  GameTooltip:SetOwner(WorldFrame, "ANCHOR_RIGHT")
  
  if not link:match("item:") then
    link = "item:"..link
  end
  GameTooltip:SetHyperlink(link)

  for i=0, GameTooltip:NumLines() do
    local left = _G["GameTooltipTextLeft"..i]
    if left and left:GetText() then
      if string.match(left:GetText(), L["Auctioning Prices:"]) then
        local right = _G["GameTooltipTextRight"..i]
        local money = right:GetText()
        local copper = 0
        local silver = 0
        local gold = 0
        money = string.sub(money, string.find(money, "[\(]+")+1, string.find(money, ")")-1)
        copper = TSMAPI:UnformatTextMoney(money)
        if (copper == nil) then
          gold = string.match(money, "([0-9]+)|")
          if not gold then return nil end
          local second = string.sub(money, string.find(money, "|t")+2)
          if string.len(second) > 0 then
            silver = string.match(second, "([0-9]+)|")
            if string.find(second, "|t") then
              local third = string.sub(second, string.find(second, "|t")+2)
              if string.len(third) > 0 then
                copper = string.match(third, "([0-9]+)|")
              else
                copper = silver
                silver = gold
                gold = 0
              end
            else
              copper = gold
              silver = 0
              gold = 0
            end
          else
            copper = gold
            silver = 0
            gold = 0
          end
          copper = gold * COPPER_PER_GOLD + silver * COPPER_PER_SILVER + copper
        end
        GameTooltip:Hide()
        return tonumber(copper)
      end
    end
  end

  GameTooltip:Hide()
  return nil
end

function private:UpdateST(fromVendor)
	if private.hidden then return end
	if InCombatLockdown() then return end

	local stData = {}
	local soldQty = 0
	local soldTotal = 0
	for bag, slot, itemString, quantity in TSMAPI:GetBagIterator(nil, true) do
	  local data = TSM.Suggestion:GetSuggestion(itemString)
		if not private.ignore[itemString] and not TSM.db.global.ignore[itemString] and (not TSM.db.char.onlyVendor or (data.suggestion and data.suggestion == TSM.Suggestion.VENDOR)) then
				local link = GetContainerItemLink(bag, slot)
				local itemName, _, itemQuality, _, _, itemType, itemSubType = GetItemInfo(link)
        local vendorSell = TSM:GetPrice("VendorSell", itemString) or 0
        local toSell = false
        local reason = TSM:QualityText(0)
        local WEAPON, ARMOR = GetAuctionItemClasses()
        if vendorSell and vendorSell > 0 then
  				if fromVendor and itemQuality and itemQuality == 0 and TSM.db.char.autoSellGray then
  				  private:SellItem(bag,slot,L["Gray"])
  				  soldTotal = soldTotal + vendorSell * quantity
  				  soldQty = soldQty + quantity
  				  toSell = false
  				else
    				if not toSell and TSM.db.char.sellSoulBound and TSMAPI:IsSoulbound(bag, slot) and itemQuality and itemQuality <= TSM.db.char.maxSoulBoundQuality and (itemType == ARMOR or itemType == WEAPON) then
    				  toSell = true
    				  reason = L["SoulBound"]
    				end
  
            local ops = TSMAPI:GetItemOperation(itemString, "Vendor")
            if not toSell and ops and #ops > 0 then
              for _, operationName in pairs(ops) do
                TSMAPI:UpdateOperation("Vendor", operationName)
                maxSellQuality = TSM.operations[operationName].maxSellQuality
                if itemQuality and itemQuality <= maxSellQuality then
                  toSell = true
                  reason = operationName..": "..L["Quality"]
                end
                if not toSell then
                  local cancelledNum, expiredNum, totalFailed = TSMAPI:ModuleAPI("Accounting","getAuctionStatsSinceLastSale",itemString)
                  if expiredNum and expiredNum >= TSM.operations[operationName].numberExpired and itemQuality <= TSM.operations[operationName].maxExpiredQuality then
                    toSell = true
                    reason = operationName..": "..expiredNum.." "..L["Expired Auctions"]
                  end
                end
                if not toSell and TSM.operations[operationName].sellMinPost then
                  local market = TSM:GetPrice("DBMarket", itemString) or 0
                  local ops2 = TSMAPI:GetItemOperation(itemString, "Auctioning")
                  if market and market > 0 and ops2 and #ops2 > 0 then
                    local minPrice = GetMinPost(link)
                    local market = TSM:GetPrice("DBMarket", itemString) or 0
                    if minPrice and minPrice > 0 and market and market > 0 then 
                      minPrice = floor((minPrice * TSM.operations[operationName].percMinPost) / 100)
                      if market < minPrice then
                        toSell = true
                        reason = operationName..": "..L["Bellow Min Post"]
                      end
                    end
                  end
                end
              end
            end
          end
                    
          if fromVendor and toSell and TSM.db.char.autoSell then
            private:SellItem(bag,slot,reason)
            soldTotal = soldTotal + vendorSell * quantity
            soldQty = soldQty + quantity
            toSell = false
          end
  
  				if toSell then
  					local row = {
  						cols = {
  							{
  								value = link
  							},
  							{
  							  value = TSMAPI:FormatTextMoneyIcon(vendorSell, "|cffffffff", true)
  							},
  							{
                  value = quantity
                },
                {},
  							{
  								value = reason
  							},
  						},
  						itemString = itemString,
  						link = link,
  						quantity = quantity,
  						bag = bag,
  						slot = slot,
  						vendorSell = vendorSell,
  					}
  					tinsert(stData, row)
  				end
  		  end
		end
	end
	
	if soldTotal and soldTotal > 0 then
	  TSM:Print(format(L["Sold %d items for %s."], soldQty, TSMAPI:FormatTextMoneyIcon(soldTotal, "|cffffffff", true)))
	end

	if #stData == 0 then
		if fromVendor and (not soldTotal or soldTotal == 0) then
			TSM:Print(L["Nothing to sell in your bags."])
		end
		private.frame.sellNextBtn:Disable()
		private.frame.sellAllBtn:Disable()
		private.frame:Hide()
	elseif fromVendor and not private.frame:IsVisible() then
    private.frame.sellNextBtn:Enable()
    private.frame.sellAllBtn:Enable()
		private.frame:Show()
	end
	private.data = CopyTable(stData)
	private.frame.st:SetData(stData)
end
