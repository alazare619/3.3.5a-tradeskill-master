local TSM = select(2, ...)
 TSM = LibStub("AceAddon-3.0"):GetAddon("TSM_Auctioning")

-- Get the Module
local Log = TSM:GetModule("Log")
local Manage = TSM:GetModule("Manage")

local Records = {}
local Display = false
local TotalCount, Mode, Skipping, Queueing

local RED = "|cffff2211"
local GREEN = "|cff22ff22"
local CYAN = "|cff99ffff"


--scanStatus.manage[2]
local function DisplaySummary()
	if not Display then return end
	local summary = RED.."---------- Summary Report: "..CYAN.."Items Processed: "..TotalCount..RED.." ----------".."|r\n\n"

	for key, record in pairs(Records) do
		if (Records[key]["action"] == "Post") or (Records[key]["action"] == "Cancel") then
		local info = Log:GetInfo(Mode, key)
		local color = TSM.Log:GetColor(Mode, key)
		local infoText = (color or "|cffffffff")..(info).."|r"
		summary = summary .. (Records[key]["count"] .." - " ..infoText.."\n" )
		end
	end
	summary = summary..(GREEN.."--- Total Items to "..Mode..": "..Queueing.." ---|r\n\n")

	for key, record in pairs(Records) do
		if (Records[key]["action"] == "Skip") then
		local info = Log:GetInfo(Mode, key)
		local color = TSM.Log:GetColor(Mode, key)
		local infoText = (color or "|cffffffff")..(info).."|r"
		summary = summary .. (Records[key]["count"] .." - " ..infoText.."\n" )
		end
	end
	summary = summary..(RED.."--- Total Items Skipped: "..Skipping.." ---|r\n\n")


	print (summary..RED.."-----------------------------------".."|r")

end

local function ClearRecords()
	wipe(Records)
	Display = false
	TotalCount = 0
	Mode = nil
	Skipping = 0
	Queueing = 0

end

local function AddRecord(self,itemString, mode, action, reason, operation, buyout)
	if (Records[reason]) then
		Records[reason]["count"] = Records[reason]["count"]+ 1
	else
		Records[reason] = {count = 1, action = action}
		Display = true
		Mode = mode
	end
	if (action  == "Skip") then
		Skipping = Skipping + 1;
		
	elseif (action == "Post") or (action == "Cancel") then
		Queueing = Queueing + 1;
	end
	TotalCount = TotalCount + 1
end

hooksecurefunc( Log,"AddLogRecord", AddRecord)
hooksecurefunc( Log, "Clear", ClearRecords)
hooksecurefunc( Manage, "ScanComplete", DisplaySummary)
