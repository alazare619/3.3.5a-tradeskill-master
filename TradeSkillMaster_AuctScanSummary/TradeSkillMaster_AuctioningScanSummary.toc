## Interface: 60000
## Title: |cff00ff00TradeSkillMaster_ScanSummary|r
## Notes: Prints a summary of TSM2 Post & Cancel auction scan results.
## Author: SLOKnightFall
## Version: v1
## Dependency: TradeSkillMaster, TradeSkillMaster_Auctioning
## X-Curse-Packaged-Version: V1.1
## X-Curse-Project-Name: TradeSkillMaster_AuctioningScanSummary
## X-Curse-Project-ID: tradeskillmaster_auctionscansumm
## X-Curse-Repository-ID: wow/tradeskillmaster_auctionscansumm/mainline

TradeSkillMaster_AuctioningScanSummary.lua
